import { combineReducers } from "redux";
import NumReducer from "../redux/Numpicker/NumReducer";
import TextReducer from "../redux/text/TextReducer";

const rootReducer = combineReducers({
    NumReducer,
  TextReducer
});
export default rootReducer;
