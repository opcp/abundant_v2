const INPUT_TEXT = "INPUT_TEXT";

const initState = {
  text: ""
};

const TextReducer = (state = initState, action) => {
  switch (action.type) {
    case INPUT_TEXT:
      return {
        ...state,
        text: action.payload
      };

    default:
      return state;
  }
};

export default TextReducer;
