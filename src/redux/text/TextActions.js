import React from "react";
import { useSelector, useDispatch } from "react-redux";

function TextActions() {
  const {text} = useSelector(state => ({
    ...state.TextReducer
  }));
  const dispatch = useDispatch();

  const input_text = e => {
    dispatch({
      type: "INPUT_TEXT",
      payload: e.target.value
    });
  };

  return (
    <>
      <input onChange={input_text} type="text" />
      <h2>{text}</h2>
    </>
  );
}

export default TextActions;
