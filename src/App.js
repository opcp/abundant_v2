import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Provider } from "react-redux";
import Header from "./components/Header/Header";
import { createStore, applyMiddleware, compose } from "redux";
import reducer from "./reducer/rootReducer";
import "./firebase/Firebase";
import thunk from "redux-thunk";
import { getFirestore, reduxFirestore } from "redux-firestore";
import { getFirebase, reactReduxFirebase } from "react-redux-firebase";
import logger from "redux-logger";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  reducer,
  composeEnhancers(
    applyMiddleware(
      thunk.withExtraArgument({ getFirebase, getFirestore }),
      logger
    )
  )
);

function App() {
  return (
    <>
      <Provider store={store}>
        <Header />
      </Provider>
    </>
  );
}

export default App;
